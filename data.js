class User {
    constructor(id, name, age, position, office, startDate) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.position = position;
        this.office = office;
        this.startDate = startDate;
    }
    checkUserById(id) {
        return this.id === id;
    }
    checkUserByAge(paramAge) {
        return this.age >= paramAge;
    }

}

let userClassList = [];

let airiSatouClass = new User(1, 'Airi Satou', 33, 'Accountant', 'Tokyo', '2008/11/28');
let itoIchiroClass = new User(2, 'Ito Ichiro', 32, 'Accountant', 'Tokyo', '2008/11/28');
let angelicaRamosClass = new User(3, 'Angelica Ramos', 47, 'Chief Executive Officer (CEO)', 'London', '2009/10/09');
let jordanPerezClass = new User(4, 'Jordan Perez', 61, 'Junior Technical Author', 'San Francisco', '2009/08/12');
let luisVargasClass = new User(5, 'Luis Vargas', 73, 'Junior Technical Author', 'San Francisco', '2009/08/12');
let mariaLopezClass = new User(6, 'Maria Lopez', 47, 'Junior Technical Author', 'San Francisco', '2009/08/12');
let nataliaAlvarezClass = new User(7, 'Natalia Alvarez', 41, 'Junior Technical Author', 'San Francisco', '2009/08/12');
let ryanKingClass = new User(8, 'Ryan King', 39, 'Junior Technical Author', 'San Francisco', '2009/08/12');
let sharonWatkinsClass = new User(9, 'Sharon Watkins', 41, 'Junior Technical Author', 'San Francisco', '2009/08/12');
let terryFoxClass = new User(10, 'Terry Fox', 39, 'Junior Technical Author', 'San Francisco', '2009/08/12');

userClassList.push(airiSatouClass);
userClassList.push(itoIchiroClass);
userClassList.push(angelicaRamosClass);
userClassList.push(jordanPerezClass);
userClassList.push(luisVargasClass);
userClassList.push(mariaLopezClass);
userClassList.push(nataliaAlvarezClass);
userClassList.push(ryanKingClass);
userClassList.push(sharonWatkinsClass);
userClassList.push(terryFoxClass);

module.exports = { userClassList };