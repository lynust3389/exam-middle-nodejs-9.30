const express = require("express");

const { userClassList } = require("./data");

const app = express();
const port = 8000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get(
  "/",
  (req, res) => {
    res.status(200).json({
      message: "User API",
      
    });
  } // end of get
);

app.get("/users", (req, res) => {
  let age = req.query.age;

  if (age) {
    let filteredUsers = [];
    for (let i = 0; i < userClassList.length; i++) {
      if (userClassList[i].checkUserByAge(age)) {
        filteredUsers.push(userClassList[i]);
      }
    }
    res.status(200).json(filteredUsers);
  } else {
    res.status(200).json(userClassList);
    console.log(userClassList);

  }
});

app.get("/users/:id", (req, res) => {
  let id = req.params.id;
  id = parseInt(id);

  let user = userClassList.find((user) => user.checkUserById(id));
  if (user) {
    res.status(200).json(user);
  } else {
    res.status(404).json({
      message: "User not found",
    });
  }
});

app.listen(port, () => {
    console.log(`User API is running on port ${port}`);
    }
);


